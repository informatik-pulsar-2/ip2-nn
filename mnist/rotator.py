import scipy.ndimage as ndimage
import numpy as np

def rotate(in_fn, out_fn):
    with open(out_fn, "w") as fw:
        with open(in_fn, "r") as fr:
            records = fr.readlines()

            for recordi in range(0, len(records)):
                record = records[recordi]
                
                all_values = record.split(',')

                label = all_values[0]
                
                imgstrarr = all_values[1:]
                imgintarr = []
                for i in imgstrarr:
                    imgintarr.append(int(i))

                img = np.reshape(imgintarr, (28, 28))

                img_p10 = ndimage.interpolation.rotate(img, 10, cval=0.01, reshape=False)
                img_m10 = ndimage.interpolation.rotate(img, -10, cval=0.01, reshape=False)

                # print("\n", label)
                # print(img)
                # print(type(img[0][0]))
                # print(img_p10)

                fw.writelines([
                    label + ',' + ",".join(str(x >= 0 and x or 0) for x in img.reshape(28 * 28)) + "\n",
                    label + ',' + ",".join(str(x >= 0 and x or 0) for x in img_p10.reshape(28 * 28)) + "\n",
                    label + ',' + ",".join(str(x >= 0 and x or 0) for x in img_m10.reshape(28 * 28)) + "\n"
                ])
            
            fr.close()
        fw.close()
    
    print("completed rotations", in_fn, out_fn)

# rotate("mnist_test_10.csv", "mnist_test_10_wRotations.csv")
# rotate("mnist_train_100.csv", "mnist_train_100_wRotations.csv")
rotate("mnist_test.csv", "mnist_test_wRotations.csv")
rotate("mnist_train.csv", "mnist_train_wRotations.csv")
