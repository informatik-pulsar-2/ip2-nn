# Informatikpulsar 2 - Neuronales Netzwerk

Die Sektion "Setup" ist für das Einrichten der Trainings- & Testdaten für das Neuronale Netzwerk, und muss nur befolgt werden, wenn das NN erneut trainiert oder getestet werden soll. Ansonsten ist in dieser Repository ein bereits trainiertes Neuronales Netzwerk vorhanden (`mn.npy`), welches automatisch geladen wird, sofern der Code nicht verändert wurde.

Zudem ist anzumerken, das alle Python-Dateien von der Root-Directory der Repository ausgeführt werden sollten.

## Setup

Das ausführen einiger Dateien benötigt die Python-Module `numpy` und `scipy`, welche durch die [Anaconda3-Distribution](https://www.anaconda.com/distribution/) direkt verfügbar sind.

1. `mnist/orig2csv.py` ausführen
2. `mnist/rotator.py` ausführen

## Ausführung

Zum benutzen des Neuronalen Netzwerks kann `nn/execute.py` ausgeführt werden.

## Eigener Input

Die Datei `custominput.png` wird, wenn während der Ausführung des Neuronalen Netzwerkes `own` angegeben wird, als Input für das Neuronale Netzwerk benutzt. Diese Datei kann mit einem einfachen Bildbearbeitungsprogramm editiert werden. Wichtig dabei ist jedoch, dass das Bild das Format 28px \* 28px haben muss, und die eigentliche Zahl in diesem Bild zu allen vier Seiten hin einige Pixel Platz braucht.

## Ressourcen

- [MNIST-Datenbank](http://yann.lecun.com/exdb/mnist/)
- Buch [Neuronale Netze selbst programmieren](https://www.oreilly.de/buecher/12892/9783960090434-neuronale-netze-selbst-programmieren.html)
