# by Noxmain
# Handschrift KIX
# -*- coding: utf-8 -*-
import numpy as np

import matplotlib.pyplot as mp
import imageio
import glob

from multilayer_network import multilayerNetwork as MN
import atexit
import random as rand

# -------------------- #
load = True # Gespeichertes Neuronales Netz laden?
save = False # Neuronales Netz speichern?
train = False # Trainieren?
# -------------------- #

# Neuronales Netz erstellen
inputnodes = 784
hiddennodes = 200
outputnodes = 10
learningrate = 0.08
nn = MN([inputnodes, hiddennodes, outputnodes], learningrate)

# Neuronales Netz sichern
def save_nn():
        print("Netzwerk sichern...")
        nn.save()

# Beim Beenden sichern
if save:
    atexit.register(save_nn)

# Neuronales Netz laden
if load:
    print("Netzwerk laden...")
    nn.load()

if train:
    print("Netzwerk trainieren...")

    # Daten laden
    data_file = open("mnist/mnist_train.csv", 'r') # -------------------- #
    data_content = data_file.readlines()
    data_file.close()

    # Für jeden Eintrag der Daten
    for item in data_content:
        # Inputs filtern
        data_values = item.split(',')
        inputs = (np.asfarray(data_values[1:]) / 255.0 * 0.99) + 0.01

        # Soll-Output erstellen
        targets = np.zeros(outputnodes) + 0.01
        targets[int(data_values[0])] = 0.99
        
        # Neuronales Netz trainieren
        if train:
            nn.train(inputs, targets)

# Testen
z = True
while z == True:
    print()
    print("Bitte Testmodus eingeben:")
    print("all >> Alle Daten testen und allgemeinen Richtigkeitswert zurückgeben")
    print("one >> Zufälligen Datenteil laden und testen")
    print("own >> cutominput.png laden und testen")
    print("Alles andere >> Programm beenden")
    mode = input("Bitte eingeben >> ")

    if mode == "all":
        # Daten Laden
        data_file = open("mnist/mnist_test.csv", 'r')
        data_content = data_file.readlines()
        data_file.close()
        
        performance_list = []
        for value_number in range(10000):
            # Inputs filtern
            data_values = data_content[value_number].split(',')
            inputs = (np.asfarray(data_values[1:]) / 255.0 * 0.99) + 0.01
            
            # Ausführen
            output = nn.query(inputs)
            
            # Überprüfen
            if int(data_values[0]) == int(np.argmax(output)):
                performance_list.append(1)
            else:
                performance_list.append(0)
        
        # Ergebnisse anzeigen
        performance = np.asarray(performance_list)
        print("Allgemeine Richtigkeit: ", (performance.sum() / performance.size) * 100, "%", sep = "")
    
    elif mode == "one":
        # Daten Laden
        data_file = open("mnist/mnist_test.csv", 'r')
        data_content = data_file.readlines()
        data_file.close()
        
        # Inputs filtern
        data_values = data_content[rand.randint(0, 10000)].split(',')
        inputs = (np.asfarray(data_values[1:]) / 255.0 * 0.99) + 0.01
        
        # Input darstellen
        data_array = np.asfarray(data_values[1:]).reshape((28, 28))
        mp.imshow(data_array, cmap = 'Greys', interpolation = 'None')
        mp.show()
        
        # Ausführen
        output = nn.query(inputs)
                
        # Ergebnisse anzeigen
        print("Label:", data_values[0], "Output:", np.argmax(output))
        y = 0
        for x in output:
            print(y, ": ", str(round(x[0], 4) * 100)[:4], "%", sep = "")
            y += 1
        
    elif mode == "own":
        # Eigenen Input laden
        for custominput in glob.glob('custominput.png'):
            print("Datei laden...")
            
            # Datei laden, konvertieren und skalieren
            data_values = imageio.imread(custominput, as_gray = True)
            inputs = ((255.0 - data_values.reshape(784)) / 255.0 * 0.99) + 0.01
                        
        # Input darstellen
        data_array = np.asfarray(255.0 - data_values).reshape((28, 28))
        mp.imshow(data_array, cmap = 'Greys', interpolation = 'None')
        mp.show()
        
        # Ausführen
        output = nn.query(inputs)
        
        # Ergebnisse anzeigen
        print("Output:", np.argmax(output))
        y = 0
        for x in output:
            print(y, ": ", str(round(x[0], 4) * 100)[:4], "%", sep = "")
            y += 1
        
    else:
        print("Netzwerk beenden...")
        z = False
    

# Neuronales Netz sichern
if save:
    save_nn()
