# by Noxmain
# -*- coding: utf-8 -*-
import numpy as np
import scipy.special as sc

class multilayerNetwork:
    # Initialisieren
    def __init__(self, nodes, learningrate):
        self.nodes = nodes
        
        # Gewichte erstellen    
        lastnode = 0
        self.w = []
        
        for node in self.nodes:
            if lastnode != 0:
                self.w.append(np.random.rand(node, lastnode) - 0.5)
            
            lastnode = node
        
        # Lernrate festlegen
        self.l = learningrate
        
        # Sigmoid-Funktion
        self.s = lambda x: sc.expit(x)

    # Trainieren
    def train(self, inputs, targets):
        # Funktioninputs speichern
        i = np.array(inputs, ndmin = 2).T
        t = np.array(targets, ndmin = 2).T
        
        # Berechnen
        outputs = [i]
        for weight in self.w:
            i = self.s(np.dot(weight, i))
            outputs.append(i)
        
        # Errors berechnen
        errors = [t - i]
        lastoutput = t - i
        for index in range(len(self.w) - 1):
            errors.append(np.dot(self.w[(len(self.w) - 1) - index].T, lastoutput))
            lastoutput = np.dot(self.w[(len(self.w) - 1) - index].T, lastoutput)
        
        # Gewichte anpassen
        for index in range(len(self.w)):
            self.w[index] += self.l * np.dot((errors[len(errors) - (index + 1)] * outputs[index + 1] * (1.0 - outputs[index + 1])), outputs[index].T)
    
    # Abfragen
    def query(self, inputs):
        # Inputs als i speichern
        i = np.array(inputs, ndmin = 2).T
        
        # Berechnen
        for weight in self.w:
            i = self.s(np.dot(weight, i))
        
        return i
    
    def save(self):
        np.save("mn.npy", [self.nodes, self.w, self.l])
    
    def load(self):
        self.nodes, self.w, self.l = np.load("mn.npy", allow_pickle=True)
